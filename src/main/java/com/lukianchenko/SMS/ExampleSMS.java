package com.lukianchenko.SMS;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

public class ExampleSMS {
    // Find your Account Sid and Token at twilio.com/user/account
    public static final String ACCOUNT_SID = "from twilio";
    public static final String AUTH_TOKEN = "from twilio";

    public static void send(String str) {
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
        Message message = Message
                .creator(new PhoneNumber("my number"), /*my phone number*/
                        new PhoneNumber("from tvilio"), str) .create(); /*attached to me number*/
    }
}
